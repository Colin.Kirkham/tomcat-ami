variable "region" {
  type    = string
  default = "us-east-2"
}

packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

data "amazon-ami" "source_ami" {
  filters = {
    name                = "amzn2-ami-hvm-*-x86_64-gp2"
    root-device-type    = "ebs"
    virtualization-type = "hvm"
  }
  most_recent = true
  owners      = ["amazon", "011834576243"]
  region      = var.region
}

source "amazon-ebs" "ami_builder" {
  ami_name      = "tomcat-instance {{timestamp}}"
  source_ami    = data.amazon-ami.source_ami.id
  instance_type = "t3.micro"
  ssh_username  = "ec2-user"
  tags = {
    Name = "Colin's Tomcat Test AMI"
  }
  region = var.region
  launch_block_device_mappings {
    delete_on_termination = true
    device_name           = "/dev/xvda"
    volume_size           = 16
  }
}

build {
  sources = ["source.amazon-ebs.ami_builder"]

  provisioner "ansible" {
    playbook_file = "playbook.yml"
  }
}
