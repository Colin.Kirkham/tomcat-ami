# Tomcat AMI Project

First run init on the packer script.

```packer init .```

Then run packer itself.

```packer build .```

That's it. You should now have a tomcat AMI instance created and ready for use.

Next you need to create an instance. 

* In the AWS console, head to the EC2 area.
* On the left, click AMIs under images.
* Click the checkbox for the AMI that you have just created. Check the creation date to ensure it's the right one.
* Click the launch button
* t2.micro will be selected by default, this is fine. Click the "Next: Configure Instance Details" button to move on.
* No changes needed here. Click the "Next: Add Storage" button to move on.
* Again, no changes. Click the "Next: Add Tags" button.
* Still no changes for this screen. Click "Next: Configure Security Group".
* The SSH rule is already added but open to the world. Find your ip address (google "whatsmyip") and enter this value into "Source" followed by "/32". This allows you to connect to the instance command line via SSH.
* Click "Add Rule", change "Port Range" to 8080 and use the same "Source" value as above. This allows you to call the servlet with your browser.
* Click "Review and Launch"
* Check your values then click Launch.

If you haven't created a keypair yet you'll be given an opportunity to do so. Otherwise just select the one you already have. Acknowledge the statement at the bottom.

* Click "Launch Instance".
* Now click on the instance link in the green "Your instances are now launching" box.
* Give the instance a while to start up. Click the refresh button if you're getting bored.
* Select your instance to find the "Public IPv4 address" entry. Open a browser and navigate to http://<-instance address->:8080/app/preferences
* Open a command line window and connect via SSH using the following command:
```ssh -i <-pem certificate file path-> ec2-user@<-instance address->```

## Useful locations

The following paths may prove useful as you're looking around.

* /usr/share/tomcat/webapps          - put war files here
* /usr/share/tomcat/conf             - the configuration directory for Tomcat
* /usr/share/tomcat/logs             - log files for tomcat
* /usr/lib/jvm/jre                   - the home directory for java as set in tomcat.conf
* /usr/lib/jvm/jre/.systemPrefs      - the directory holding system preferences
* /usr/share/tomcat/.java/.userPrefs - the directory holding user preferences
